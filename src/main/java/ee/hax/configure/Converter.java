package ee.hax.configure;

import ee.hax.component.Component;
import ee.hax.component.ComponentType;
import ee.hax.person.Person;
import io.spring.guides.gs_producing_web_service.ComponentTypeRestriction;

import java.util.Optional;

/**
 * Created by karl-martin on 12/10/16.
 */
public class Converter {

    public static io.spring.guides.gs_producing_web_service.Component convertComponent(Component component) {
        io.spring.guides.gs_producing_web_service.Component soapComponent =
                new io.spring.guides.gs_producing_web_service.Component();
        soapComponent.setId(component.getId());
        soapComponent.setCompanyName(component.getCompanyName());
        soapComponent.setComponentType(component.getComponentType().toString());
        soapComponent.setPrice(component.getPrice());
        soapComponent.setProductName(component.getProductName());
        return soapComponent;
    }

    public static io.spring.guides.gs_producing_web_service.Person convertPerson(Optional<Person> personOptional) {
        io.spring.guides.gs_producing_web_service.Person soapPerson =
                new io.spring.guides.gs_producing_web_service.Person();
        if (personOptional.isPresent()){
            Person person = personOptional.get();
            soapPerson.setAge(person.getAge());
            soapPerson.setFirstName(person.getFirstName());
            soapPerson.setId(person.getId());
            soapPerson.setLastName(person.getLastName());
            if (person.getComponents() != null) {
                for (Component component: person.getComponents()) {
                    soapPerson.getComponents().add(convertComponent(component));
                }
            }
        }
        return soapPerson;
    }

    public static ComponentType restrictionToComponentType(ComponentTypeRestriction componentType) {
        ComponentType[] componentTypes = ComponentType.values();
        for (int i = 0; i < componentTypes.length; i++) {
            if(componentTypes[i].toString().equals(componentType.value())){
                return componentTypes[i];
            }
        }
        return null;
    }

    //public static io.spring.guides.gs_producing_web_service.Person
}
