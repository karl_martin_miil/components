package ee.hax.soap;

import ee.hax.component.Component;
import ee.hax.component.ComponentRepository;
import ee.hax.component.ComponentService;
import ee.hax.configure.Converter;
import ee.hax.person.Person;
import ee.hax.person.PersonRepository;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Endpoint
public class ComponentEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	List<String> indempotentCheckList = new ArrayList<>();

	private boolean doIndempotentCheck(String token) {
		for (String item : indempotentCheckList) {
			if (item.equals(token)) {
				return false;
			}
		}
		indempotentCheckList.add(token);
		return true;
	}


	private boolean authcheck(MessageContext messageContext) {
		SaajSoapMessage headerRequest = (SaajSoapMessage) messageContext.getRequest();
		javax.xml.soap.MimeHeaders mimeHeaders = headerRequest.getSaajMessage().getMimeHeaders();
		if(mimeHeaders.getHeader("auth").length > 0){
			return true;
		}
		return false;
	}


	@Autowired
	ComponentRepository componentRepository;
	@Autowired
	PersonRepository personRepository;
	@Autowired
	ComponentService componentService;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getComponentRequest")
	@ResponsePayload
	public GetComponentResponse getComponent(@RequestPayload GetComponentRequest request, MessageContext messageContext) {
		GetComponentResponse response = new GetComponentResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())){
			response.setComponent(Converter.convertComponent(componentRepository.findOne(request.getId())));
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllComponentsRequest")
	@ResponsePayload
	public GetAllComponentsResponse getAllComponents(@RequestPayload GetAllComponentsRequest request, MessageContext messageContext) {
		GetAllComponentsResponse response = new GetAllComponentsResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())){
			Set<Component> components = componentService.getAllComponents(
					request.getCompanyName()!=null? request.getCompanyName(): "",
					request.getProductName()!=null? request.getProductName(): "",
					request.getPrice()!=null? request.getPrice(): "");
			components.forEach(component -> response.getComponent().add(Converter.convertComponent(component)));
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllComponentsOfPersonRequest")
	@ResponsePayload
	public GetAllComponentsOfPersonResponse getAllComponentsOfPerson(@RequestPayload GetAllComponentsOfPersonRequest request, MessageContext messageContext) {
		GetAllComponentsOfPersonResponse response = new GetAllComponentsOfPersonResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())){
			Optional<Person> person = personRepository.findOne(request.getPersonId());
			if(person.isPresent()) {
				Set<Component> components = person.get().getComponents();
				components.forEach(component -> response.getComponent().add(Converter.convertComponent(component)));
			}
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPersonRequest")
	@ResponsePayload
	public GetPersonResponse getPerson(@RequestPayload GetPersonRequest request, MessageContext messageContext) {
		GetPersonResponse response = new GetPersonResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())) {
			response.setPerson(Converter.convertPerson(personRepository.findOne(request.getId())));
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPersonsRequest")
	@ResponsePayload
	public GetAllPersonsResponse getAllPersons(@RequestPayload GetAllPersonsRequest request, MessageContext messageContext) {
		GetAllPersonsResponse response = new GetAllPersonsResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())) {
			Set<ee.hax.person.Person> persons = personRepository.findAll();
			for (Person person : persons) {
				response.getPerson().add(Converter.convertPerson(Optional.of(person)));
			}
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPersonRequest")
	@ResponsePayload
	public AddPersonResponse addPerson(@RequestPayload AddPersonRequest request, MessageContext messageContext) {
		AddPersonResponse response = new AddPersonResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())) {
			Person person = new Person(request.getFirstName(), request.getLastName(), request.getAge());
			person = personRepository.save(person);
			response.setPerson(Converter.convertPerson(Optional.of(person)));
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addComponentRequest")
	@ResponsePayload
	public AddComponentResponse addComponent(@RequestPayload AddComponentRequest request, MessageContext messageContext) {
		AddComponentResponse response = new AddComponentResponse();
		if(authcheck(messageContext) && doIndempotentCheck(request.getToken())) {
			ee.hax.component.Component component = new Component(request.getCompanyName(),
					request.getComponentType(), request.getPrice(), request.getProductName());
			component = componentService.addComponentToPerson(component, request.getPersonId());
			response.setComponent(Converter.convertComponent(component));
		}
		return response;
	}
}