package ee.hax.component;

import ee.hax.configure.Converter;
import io.spring.guides.gs_producing_web_service.ComponentTypeRestriction;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by karl-martin on 11/28/16.
 */
@Getter
@Setter
@ToString
@Entity
public class Component {

    @Id
    @GeneratedValue
    private long id;

    int price;

    String productName;

    String companyName;

    ComponentType componentType;

    public Component() {
    }

    public Component(String companyName, ComponentTypeRestriction componentType, int price, String productName) {
        this.componentType = Converter.restrictionToComponentType(componentType);
        this.companyName = companyName;
        this.productName = productName;
        this.price = price;
    }
}
