package ee.hax.component;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;

/**
 * Created by karl-martin on 12/10/16.
 */
@RepositoryRestResource(exported = false)
public interface ComponentRepository extends CrudRepository<Component, Long> {
    Set<Component> findAll();

    Component findByProductName(String name);


    @Query("SELECT C FROM Component C" +
            " WHERE C.companyName LIKE CONCAT('%',:companyName,'%')"+
            " AND C.productName LIKE CONCAT('%',:productName,'%')"
    )
    Set<Component> findByParameters(@Param("companyName") String companyName,
                                    @Param("productName") String productName);
}
