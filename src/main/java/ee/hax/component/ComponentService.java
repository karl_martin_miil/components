package ee.hax.component;

import ee.hax.person.Person;
import ee.hax.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by karl-martin on 11/28/16.
 */
@Service
public class ComponentService {

    @Autowired
    ComponentRepository componentRepository;
    @Autowired
    PersonRepository personRepository;


    public Component addComponentToPerson(Component component, long personId) {
        Optional<Person> personOptional = personRepository.findOne(personId);
        if(personOptional.isPresent()){
            Person person = personOptional.get();
            person.getComponents().add(component);
            personRepository.save(person);
            return component;
        }
        return null;
    }

    public Set<Component> getComponentsOfPerson(long personId) {
        Optional<Person> personOptional = personRepository.findOne(personId);
        if (personOptional.isPresent()) {
            Person person = personOptional.get();
            return person.getComponents();
        }
        return null;
    }

    public Component getComponentById(long componentId) {
        return componentRepository.findOne(componentId);
    }


    public Set<Component> getAllComponents(String companyName, String productName, String price) {
        return componentRepository.findByParameters(companyName, productName).stream()
                .filter(component -> price.equals("") || price.equals(component.getPrice()+"")).collect(Collectors.toSet());
    }
}
