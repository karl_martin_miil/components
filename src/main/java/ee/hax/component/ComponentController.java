package ee.hax.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * Created by karl-martin on 11/28/16.
 */
@RestController
public class ComponentController {

    @Autowired
    ComponentService componentService;

    @RequestMapping(value = "/persons/{id}/components", method = RequestMethod.POST)
    public Component addComponentToPerson(@RequestHeader(value="auth", required = true) String auth,
                                          @RequestBody Component component, @PathVariable(value = "id") int personId) {
        return componentService.addComponentToPerson(component, personId);
    }

    @RequestMapping(value = "/persons/{id}/components", method = RequestMethod.GET)
    public Set<Component> getPersonsComponents(@RequestHeader(value="auth", required = true) String auth,
                                               @PathVariable(value = "id") long personId) {
        return componentService.getComponentsOfPerson(personId);
    }

    @RequestMapping(value = "/components/{id}", method = RequestMethod.GET)
    public Component getComponentById(@RequestHeader(value="auth", required = true) String auth,
                                      @PathVariable(value = "id") long componentId) {
        return componentService.getComponentById(componentId);
    }

    @RequestMapping(value = "/components", method = RequestMethod.GET)
    public Set<Component> getAllComponent(@RequestHeader(value="auth", required = true) String auth,
                                      @RequestParam(required=false, defaultValue = "") String companyName,
                                      @RequestParam(required=false, defaultValue = "") String price,
                                      @RequestParam(required=false, defaultValue = "") String productName) {
        return componentService.getAllComponents(companyName, productName, price);
    }

}
