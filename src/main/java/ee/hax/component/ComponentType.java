package ee.hax.component;

/**
 * Created by karl-martin on 12/10/16.
 */
public enum ComponentType {
    KEYBOARD, MOUSE, DISPLAY
}
