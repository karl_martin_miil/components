package ee.hax.person;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;
import java.util.Set;

/**
 * Created by karl-martin on 12/10/16.
 */
@RepositoryRestResource(exported = false)
public interface PersonRepository extends CrudRepository<Person, Long> {

    Set<Person> findAll();

    Optional<Person> findOne(long personId);

}
