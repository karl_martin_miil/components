package ee.hax.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * Created by karl-martin on 12/10/16.
 */
@RestController
public class PersonController {

    @Autowired
    PersonService personService;

    @RequestMapping(value = "/persons", method = RequestMethod.POST)
    public Person addComponentToPerson(@RequestHeader(value="auth", required = true) String auth,
                                       @RequestBody Person person) {
        return personService.addPerson(person);
    }

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public Set<Person> getAllPersons(@RequestHeader(value="auth", required = true) String auth) {
        return personService.getAllPersons();
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.GET)
    public Person getPersonById(@RequestHeader(value="auth", required = true) String auth,
                                @PathVariable(value = "id") long personId) {
        return personService.getOnePerson(personId);
    }
}
