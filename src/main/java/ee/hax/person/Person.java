package ee.hax.person;

import ee.hax.component.Component;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by karl-martin on 11/28/16.
 */
@Getter
@Setter
@Entity
public class Person
{

    @Id
    @GeneratedValue
    private long id;

    String firstName;

    String lastName;

    int age;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name="person_component_rel",
            joinColumns=@JoinColumn(name="person_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="component_id", referencedColumnName="id"))
    private Set<Component> components;

    public Person() {
    }

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

}
