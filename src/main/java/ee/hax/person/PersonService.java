package ee.hax.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

/**
 * Created by karl-martin on 12/10/16.
 */
@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public Set<Person> getAllPersons() {
        return personRepository.findAll();
    }

    public Person addPerson(Person person) {
        return personRepository.save(person);
    }

    public Person getOnePerson(long personId) {
        Optional<Person> personOptional = personRepository.findOne(personId);
        if (personOptional.isPresent()) {
            return personOptional.get();
        }
        return null;
    }
}
